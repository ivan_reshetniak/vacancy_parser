package com.project.vacancy_parser.config;

import com.project.vacancy_parser.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthHandler implements AuthenticationSuccessHandler {

    @Autowired
    private UserAccountService userAccountService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {

        OAuth2AuthenticationToken auth = (OAuth2AuthenticationToken) authentication;
        userAccountService.addUserToDB(auth);
        httpServletResponse.sendRedirect("/");
    }
}
