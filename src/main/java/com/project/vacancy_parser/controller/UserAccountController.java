package com.project.vacancy_parser.controller;

import com.project.vacancy_parser.dto.UserAccountDTO;
import com.project.vacancy_parser.dto.VacancyFormDTO;
import com.project.vacancy_parser.entity.UserAccount;
import com.project.vacancy_parser.service.UserAccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class UserAccountController {
    private final UserAccountService userAccountService;


    @GetMapping
    public List<UserAccount> getAllUsers() {
        return userAccountService.getAllUsers();
    }

    @GetMapping(value = "/profile")
    public UserAccountDTO getProfile(OAuth2AuthenticationToken auth) {
        Map<String, Object> attributes = auth.getPrincipal().getAttributes();

        String name = (String) attributes.get("name");
        String email = (String) attributes.get("email");
        String picture = (String) attributes.get("picture");

        return new UserAccountDTO(name, email, picture);
    }

    @PostMapping(value = "/addSubscription")
    public void addSubscription(@RequestBody VacancyFormDTO vacancyFormDTO, OAuth2AuthenticationToken auth) {
        userAccountService.addSubscription(vacancyFormDTO, auth);
    }
}
