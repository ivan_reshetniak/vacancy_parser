package com.project.vacancy_parser.controller;

import com.project.vacancy_parser.dto.VacancyFormDTO;
import com.project.vacancy_parser.entity.Vacancy;
import com.project.vacancy_parser.service.VacancyService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping (value = "/api/vacancies", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class VacancyController {
    private final VacancyService vacancyService;



    @GetMapping (value = "/keyword")
    public List<Vacancy> getVacanciesByKeyword (@RequestBody Map<String, String> map){
        return vacancyService.findVacanciesByKeyword(map.get("keyword"));
    }

    @PostMapping (value = "/form")
    public List<Vacancy> getVacanciesByForm (@RequestBody VacancyFormDTO vacancyFormDTO){
        return vacancyService.findVacancies(vacancyFormDTO);
    }

    @PostMapping (value = "/save")
    public void saveVacancy (@RequestBody Vacancy vacancy, OAuth2AuthenticationToken auth){
        System.out.println(vacancy.getCityName() + vacancy.getName() + vacancy.getDate() + "TEST!!!!!!!!!!!!");
        vacancyService.saveVacancy(vacancy, auth);
    }

    @GetMapping (value = "/saved")
    public List<Vacancy> getSavedVacancies (OAuth2AuthenticationToken auth){
        return vacancyService.getVacanciesByUser(auth);
    }
}
