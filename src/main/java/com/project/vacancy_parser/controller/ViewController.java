package com.project.vacancy_parser.controller;

import com.project.vacancy_parser.dto.VacancyFormDTO;
import com.project.vacancy_parser.entity.Vacancy;
import com.project.vacancy_parser.service.UserAccountService;
import com.project.vacancy_parser.service.VacancyService;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/")
@AllArgsConstructor
public class ViewController {
    private final VacancyService vacancyService;
    private final UserAccountService userAccountService;


    @GetMapping
    public String getIndex(Model model) {
        VacancyFormDTO vacancyFormDTO = new VacancyFormDTO();
        model.addAttribute("vacancyFormDTO", vacancyFormDTO);
        return "index";
    }

    @GetMapping(value = "/login.html")
    public String loginPage() {
        return "login";
    }

    @GetMapping(value = "/vacancies")
    public String getVacancies(Model model, @ModelAttribute("vacancyFormDTO") VacancyFormDTO vacancyFormDTO){
        List<Vacancy> vacancyList = vacancyService.findVacancies(vacancyFormDTO);
        model.addAttribute("vacancyList", vacancyList);
        return "vacancyList";
    }

    @GetMapping(value = "/vacancies/saved")
    public String getSavedVacancies(Model model, OAuth2AuthenticationToken auth){
        List<Vacancy> vacancyList = userAccountService.getUserVacancyList(auth);
        model.addAttribute("vacancyList", vacancyList);
        return "vacancyList";
    }

    @GetMapping(value = "/oldindex")
    public String getTestIndex(){
        return "oldIndex";
    }

    @GetMapping(value = "/profilePage")
    public String getProfilePage() {
        return "profilePage";
    }
}
