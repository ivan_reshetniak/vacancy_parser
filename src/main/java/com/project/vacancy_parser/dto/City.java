package com.project.vacancy_parser.dto;

public enum City {
    All(0) ,Kyiv(1), Lviv(2), Odesa(3), Dnipro(4), Vinnytsia(5), Zhytomyr(7), Ivano_Frankivsk(10);

    private final int id;

    private City(int id) {
        this.id = id;
    }

    public int getValue() {
        return this.id;
    }
}
