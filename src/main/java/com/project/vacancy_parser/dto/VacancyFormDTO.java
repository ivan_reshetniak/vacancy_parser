package com.project.vacancy_parser.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VacancyFormDTO {
    private City city;
    private String keyword;
    private boolean sortByDate;
}
