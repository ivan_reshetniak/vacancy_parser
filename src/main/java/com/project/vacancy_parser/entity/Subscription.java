package com.project.vacancy_parser.entity;

import com.project.vacancy_parser.dto.City;
import com.project.vacancy_parser.dto.VacancyFormDTO;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Subscription {
    @Id
    @GeneratedValue
    private Long id;
    private City city;
    private String keyword;
    private boolean sortByDate;


    public static Subscription toEntity (VacancyFormDTO vacancyFormDTO) {
        Subscription subscription = new Subscription();
        subscription.setCity(vacancyFormDTO.getCity());
        subscription.setKeyword(vacancyFormDTO.getKeyword());
        subscription.setSortByDate(vacancyFormDTO.isSortByDate());
        return subscription;
    }

    public static VacancyFormDTO toDto (Subscription subscription) {
        VacancyFormDTO vacancyFormDTO = new VacancyFormDTO();
        vacancyFormDTO.setCity(subscription.getCity());
        vacancyFormDTO.setKeyword(subscription.getKeyword());
        vacancyFormDTO.setSortByDate(subscription.isSortByDate());
        return vacancyFormDTO;
    }
}
