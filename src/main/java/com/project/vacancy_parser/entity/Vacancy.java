package com.project.vacancy_parser.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vacancy {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private LocalDate date;
    private boolean hot;
    private int salary;
    private String cityName;

    @JsonProperty(value = "notebookId")
    private long companyId;
    private String companyName;
    private String shortDescription;

    @ManyToOne
    @JoinColumn (name = "user_account_id")
    @JsonBackReference
    private UserAccount userAccount;
}
