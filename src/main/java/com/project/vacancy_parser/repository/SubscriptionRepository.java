package com.project.vacancy_parser.repository;

import com.project.vacancy_parser.entity.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionRepository extends JpaRepository <Subscription, Long> {
}
