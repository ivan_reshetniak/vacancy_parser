package com.project.vacancy_parser.repository;

import com.project.vacancy_parser.entity.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
    Optional<UserAccount> findUserAccountByEmail (String email);
    boolean existsByEmail (String email);
}
