package com.project.vacancy_parser.repository;

import com.project.vacancy_parser.entity.Vacancy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VacancyRepository extends JpaRepository <Vacancy, Long> {
}
