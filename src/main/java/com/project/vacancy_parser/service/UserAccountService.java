package com.project.vacancy_parser.service;

import com.project.vacancy_parser.dto.VacancyFormDTO;
import com.project.vacancy_parser.entity.Subscription;
import com.project.vacancy_parser.entity.UserAccount;
import com.project.vacancy_parser.entity.Vacancy;
import com.project.vacancy_parser.repository.UserAccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserAccountService {
    private UserAccountRepository userAccountRepository;

    public void addUserToDB (OAuth2AuthenticationToken auth) {
        OAuth2User auth2User = auth.getPrincipal();
        Map<String, Object> attr = auth2User.getAttributes();

        if(userAccountRepository.existsByEmail((String) attr.get("email"))) return;

        UserAccount user = new UserAccount();
        user.setName((String) attr.get("name"));
        user.setEmail((String) attr.get("email"));

        userAccountRepository.save(user);
    }

    public UserAccount getUser (OAuth2AuthenticationToken auth) {
        return userAccountRepository.findUserAccountByEmail((String) auth.getPrincipal().getAttributes().get("email"))
                .orElseThrow(() -> new RuntimeException("User is not exist in database"));
    }

    public void saveUser (UserAccount userAccount) {
        userAccountRepository.save(userAccount);
    }

    public List<UserAccount> getAllUsers() {
        return userAccountRepository.findAll();
    }

    public List<Vacancy> getUserVacancyList (OAuth2AuthenticationToken auth) {
        return userAccountRepository.findUserAccountByEmail((String) auth.getPrincipal().getAttribute("email"))
                .orElseThrow(() -> new RuntimeException("User not found"))
                .getVacancyList();
    }

    public void addSubscription (VacancyFormDTO vacancyFormDTO, OAuth2AuthenticationToken auth) {
        UserAccount userAccount = getUser(auth);

        userAccount.getSubscriptions().add(Subscription.toEntity(vacancyFormDTO));
        userAccountRepository.save(userAccount);
    }

    public List<VacancyFormDTO> getUserSubscriptionList (String email) {
        return userAccountRepository.findUserAccountByEmail(email)
                .orElseThrow(() -> new RuntimeException("User not found"))
                .getSubscriptions().stream()
                .map(Subscription::toDto)
                .collect(Collectors.toList());
    }
}
