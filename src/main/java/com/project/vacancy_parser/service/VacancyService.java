package com.project.vacancy_parser.service;

import com.project.vacancy_parser.dto.VacancyFormDTO;
import com.project.vacancy_parser.entity.Data;
import com.project.vacancy_parser.entity.UserAccount;
import com.project.vacancy_parser.entity.Vacancy;
import com.project.vacancy_parser.repository.VacancyRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Service
@AllArgsConstructor
public class VacancyService {
    private final VacancyRepository vacancyRepository;
    private final WebClient webClient = WebClient.create();
    private final UserAccountService userAccountService;


    public Vacancy findVacancyById(long id) {
        return vacancyRepository.findById(id).orElseThrow(() -> new RuntimeException("Vacancy does not exist"));
    }

    public List<Vacancy> findVacanciesByKeyword(String keyword) {
        Data data = webClient.get()
                .uri("https://api.rabota.ua/vacancy/search?keyWords=" + keyword)
                .retrieve()
                .bodyToMono(Data.class)
                .block();
        try {
            return data.getVacancyList();
        } catch (NullPointerException e) {
            throw new RuntimeException("Data not found");
        }
    }

    public List<Vacancy> findVacancies(VacancyFormDTO vacancyFormDTO) {
//        String url = "https://api.rabota.ua/vacancy/search?";

        String url = String.format("https://api.rabota.ua/vacancy/search?count=50&cityId=%d&keyWords=%s&sortBy=%s",
                vacancyFormDTO.getCity() == null ? 0 : vacancyFormDTO.getCity().getValue(),
                vacancyFormDTO.getKeyword(), vacancyFormDTO.isSortByDate() ? "Date" : "");

        Data data = webClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(Data.class)
                .block();

        assert data != null;
        return data.getVacancyList();
    }

    public void saveVacancy (Vacancy vacancy, OAuth2AuthenticationToken auth) {
        UserAccount user = userAccountService.getUser(auth);
        vacancy.setUserAccount(user);
        user.getVacancyList().add(vacancy);

        userAccountService.saveUser(user);
    }

    public List<Vacancy> getVacanciesByUser (OAuth2AuthenticationToken auth) {
        UserAccount user = userAccountService.getUser(auth);

        return user.getVacancyList();
    }
}
