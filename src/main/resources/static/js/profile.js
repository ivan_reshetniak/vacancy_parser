$(document).ready(function(){
    $.getJSON('/api/users/profile', function(data) {
        $('#name').text(data.name);
        $('#email').text(data.email);
        $("#avatar").attr("src", data.picture);
    });

    loadData();
});

function loadData() {
    $("#savedData > tbody").empty();

    $.getJSON('/api/vacancies/saved', function(data) {
        var i;

        for (i = 0; i < data.length; i++) {
            $('#savedData > tbody:last-child').append(
                $('<tr>')
                    .append(
                        $('<td>').append(
                            $('<input>').attr('type', 'checkbox').attr('value', data[i].id)
                        )
                    )
                    .append($('<td>').append(i))
                    .append($('<td>').append(data[i].id))
                    .append($('<td>').append(data[i].name))
                    .append($('<td>').append(data[i].date))
                    .append($('<td>').append(data[i].hot))
                    .append($('<td>').append(data[i].salary))
                    .append($('<td>').append(data[i].cityName))
                    .append($('<td>').append(data[i].companyName))
                    .append($('<td>').append(data[i].shortDescription))
            );
        }
    });
}