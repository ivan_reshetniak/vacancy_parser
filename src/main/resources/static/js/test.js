$(document).ready(function () {
    // searchForm();
});

function searchForm() {
    $("#searchForm").click(function () {
        var vacancyFormDTO = {
            city: $("#city").val(),
            keyword: $("#keyword").val(),
            sortByDate: $("sortByDate").val() == "1"
        }

        $.ajax({
            type: "POST",
            url: "/api/vacancies/form",
            contentType: "application/json",
            data: JSON.stringify(vacancyFormDTO),
            dataType: "json",
            success: function (data) {
                $("#data > tbody").empty();

                var i;
                // var button = document.createElement("button");

                for (i = 0; i < data.length; i++) {
                    $('#data > tbody:last-child').append(
                        $('<tr>')
                            .append($('<td>').append(i + 1))
                            .append($('<td>').append(data[i].id))
                            .append($('<td>').append(data[i].name))
                            .append($('<td>').append(data[i].date))
                            .append($('<td>').append(data[i].hot))
                            .append($('<td>').append(data[i].salary))
                            .append($('<td>').append(data[i].cityName))
                            .append($('<td>').append(data[i].companyName))
                            .append($('<td>').append(data[i].shortDescription))
                            .append($('<td>')
                                .append($('<form>').attr('id', 'saveForm' + i)
                                .append($('<input>').attr('type', 'hidden').attr('id', 'id' + i).attr('name', 'id').attr('value', data[i].id))
                                .append($('<input>').attr('type', 'hidden').attr('id', 'name'+ i).attr('name', 'name').attr('value', data[i].name))
                                .append($('<input>').attr('type', 'hidden').attr('id', 'date'+ i).attr('name', 'date').attr('value', data[i].date))
                                .append($('<input>').attr('type', 'hidden').attr('id', 'hot'+ i).attr('name', 'hot').attr('value', data[i].hot))
                                .append($('<input>').attr('type', 'hidden').attr('id', 'salary'+ i).attr('name', 'salary').attr('value', data[i].salary))
                                .append($('<input>').attr('type', 'hidden').attr('id', 'cityName'+ i).attr('name', 'cityName').attr('value', data[i].cityName))
                                .append($('<input>').attr('type', 'hidden').attr('id', 'notebookId'+ i).attr('name', 'notebookId').attr('value', data[i].notebookId))
                                .append($('<input>').attr('type', 'hidden').attr('id', 'companyName'+ i).attr('name', 'companyName').attr('value', data[i].companyName))
                                .append($('<input>').attr('type', 'hidden').attr('id', 'shortDescription'+ i).attr('name' , 'shortDescription').attr('value', data[i].shortDescription))
                                // .append($('<button>').attr('type', 'button').attr('class', 'btn btn-primary btn-lg').attr('name', 'save').click(save()))
                                .append('<button type="button" class="btn btn-primary btn-lg" name="save" onclick="testSave()">save</button>'))
                    ));
                }
            }
        })
    })
}
//
// function save() {
//     $("#saveForm").click(function () {
//
//         var vacancy = {
//             id: $("#id").val(),
//             name: $("#name").val(),
//             date: $("#date").val(),
//             hot: $("#hot").val() == "true",
//             salary: $("#salary").val(),
//             cityName: $("#cityName").val(),
//             notebookId: $("#notebookId").val(),
//             companyName: $("#companyName").val(),
//             shortDescription: $("#shortDescription").val()
//         }
//
//         $.ajax({
//             type: "POST",
//             url: "/api/vacancies/save",
//             contentType: "application/json",
//             data: JSON.stringify(vacancy),
//             dataType: "json",
//             success: function (){
//
//             }
//         })
//     })
// }

function testSave() {
    $('button[name=save]').click(function () {
        if($(this).data('clicked')){
            return false;
        }
        else {
            $(this).data('clicked', true);
            setTimeout(function(){
                $('.one-click').data('clicked', false)
            }, 1000);
        }
        var $formElem = $(this).closest('form')
        var data = $formElem.serializeArray();

        var vacancy = {};

        $(data).each(function(i, field){
            vacancy[field.name] = field.value;
        });
        console.log(vacancy)

        $.ajax({
            type: "POST",
            url: "/api/vacancies/save",
            contentType: "application/json",
            data: JSON.stringify(vacancy),
            dataType: "json",
            success: function (){
                requestSent = false;
            }
        })
    })
}